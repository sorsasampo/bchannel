package bchannel

import (
	"sync"
)

type BroadcastChannel struct {
	// The broadcast channel. Closing this closes and removes all
	// listeners as well.
	Ch chan<- interface{}

	// Set of subscribed channels.
	listeners map[chan<- interface{}]struct{}

	lock sync.Mutex
}

func NewBroadcastChannel() *BroadcastChannel {
	bc := &BroadcastChannel{}
	c := make(chan interface{})
	bc.Ch = c
	bc.listeners = make(map[chan<- interface{}]struct{})

	go func() {
		for v := range c {
			func() {
				bc.lock.Lock()
				defer bc.lock.Unlock()
				for ch, _ := range bc.listeners {
					ch <- v
				}
			}()
		}

		func() {
			bc.lock.Lock()
			defer bc.lock.Unlock()
			for listener := range bc.listeners {
				close(listener)
			}
			bc.listeners = make(map[chan<- interface{}]struct{})
		}()
	}()

	return bc
}

func (bc *BroadcastChannel) Listen(buffer int) chan interface{} {
	bc.lock.Lock()
	defer bc.lock.Unlock()
	c := make(chan interface{}, buffer)
	bc.listeners[c] = struct{}{}
	return c
}

func (bc *BroadcastChannel) Unlisten(c chan interface{}) {
	bc.lock.Lock()
	defer bc.lock.Unlock()
	delete(bc.listeners, c)
}
