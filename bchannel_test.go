package bchannel

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestBroadcastingOne(t *testing.T) {
	assert := assert.New(t)

	bc := NewBroadcastChannel()

	c1 := bc.Listen(1)

	bc.Ch <- 1
	bc.Ch <- 2
	close(bc.Ch)

	assert.Equal(1, <-c1)
	assert.Equal(2, <-c1)

	select {
	case <-c1:
	default:
		t.Errorf("c1 should be closed")
	}
}

func TestBroadcastingTwo(t *testing.T) {
	assert := assert.New(t)

	bc := NewBroadcastChannel()
	c1 := bc.Listen(2)
	c2 := bc.Listen(2)

	bc.Ch <- 1
	bc.Ch <- 2

	assert.Equal(1, <-c1)
	assert.Equal(2, <-c1)
	assert.Equal(1, <-c2)
	assert.Equal(2, <-c2)

	bc.Unlisten(c1)
	bc.Unlisten(c2)

	assert.Equal(0, len(bc.listeners))
}

func TestBroadcastClose(t *testing.T) {
	assert := assert.New(t)

	bc := NewBroadcastChannel()
	bc.Listen(0)

	close(bc.Ch)
	// Wait for goroutine to clear listeners
	time.Sleep(10 * time.Millisecond)
	assert.Equal(0, len(bc.listeners), "Close should remove listeners")
}

func testRegister(t *testing.T) {
	assert := assert.New(t)

	bc := NewBroadcastChannel()
	c1 := bc.Listen(0)

	assert.Equal(1, len(bc.listeners))

	bc.Unlisten(c1)
	assert.Equal(0, len(bc.listeners))
}
